#!/usr/bin/env python3

import serial
import time
from time import sleep
from paho.mqtt import client as mqtt_client
import os
import json
from dotenv import load_dotenv

load_dotenv()

broker = os.getenv('BROKER')
port = int(os.getenv('PORT'))
topic = os.getenv('TOPIC')
client_id = os.getenv('CLIENT_ID')
username = os.getenv('USERNAME')
password = os.getenv('PASSWORD')
plant_id = os.getenv("PLANT_ID")


def on_message(client, userdata, msg):
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.reset_input_buffer()
    if msg.topic == "topic/watering_mode":
        m_decode=str(msg.payload.decode("utf-8","ignore"))
        print("data Received type",type(m_decode))
        print("data Received",m_decode)
        print("Converting from Json to Object")
        plant = json.loads(m_decode) #decode json data
        
        if plant["wateringMode"] == "AUTO":
            ser.write(b'a')
            str_thresholdUp = str(plant["humidityThresholdUp"])
            str_thresholdDown = str(plant["humidityThresholdDown"])
            ser.write(str_thresholdUp.encode())
            ser.write(str_thresholdDown.encode())
            
        if plant["wateringMode"] == "MANUAL":
            ser.write(b'm')
            str_wateringAmount = str(plant["wateringAmount"])
            ser.write(str_wateringAmount.encode())

    

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(broker, port)
    client.subscribe('topic/manual')
    client.subscribe('topic/automatic')
    return client
    

def publish(client, topic, msg):
    new_msg = {}
    if topic == "topic/plant_info":
         new_msg["id"] = plant_id
         new_msg["humidity"] = msg["moisture1"]
         new_msg["temperature"] = msg["temp"]
    elif topic == "topic/room_info":
         new_msg["id"] = plant_id
         new_msg["humidity"] = msg["moisture1"]
         new_msg["temperature"] = msg["temp"]
         new_msg["lighting"] = msg["lighting"]
    elif topic == "topic/water_tank_info":
         new_msg["id"] = plant_id
         new_msg["waterLevel"] = msg["distance"]
         
    result = client.publish(topic, json.dumps(new_msg))
    
    if result[0] == 0:
        print(f"Send `{new_msg}` to topic `{topic} successfully.`")
    else:
        print(f"Failed to send message to topic {topic}")
        
        
def main():
    client = connect_mqtt()
    client.loop_start()
    
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.reset_input_buffer()
    ser.write(b'j')
    while True:
        if ser.in_waiting > 0:
            data = ser.readline().decode("utf-8")
            ser.write(b'j')
            json_data = None
            try:
                json_data = json.loads(data)
            except json.JSONDecodeError as e:
                print("JSON:", e)
            if json_data != None:
                print(json_data)
                publish(client, "topic/plant_info", json_data)
                publish(client, "topic/room_info", json_data)
                publish(client, "topic/water_tank_info", json_data)
            sleep(5)

S
if __name__ == '__main__':
    main()


